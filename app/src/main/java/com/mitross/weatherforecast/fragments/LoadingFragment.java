package com.mitross.weatherforecast.fragments;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airbnb.lottie.LottieAnimationView;
import com.mitross.weatherforecast.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link BaseFragment} subclass.
 */
public class LoadingFragment extends BaseFragment {



    public static final String TAG = LoadingFragment.class.getSimpleName();
    private static Fragment fragment = null;
    Unbinder unbinder;
    private AsyncTask forecastTask = null;

    public LoadingFragment() {
        // Required empty public constructor
    }

    @Override
    public Integer layoutId() {
        return R.layout.fragment_loading;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initFragment();
    }

    private void initFragment() {
        forecastTask = new ForecastTask().execute();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private class ForecastTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            callApiToGetForecast(23.02, 72.57);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    public static Fragment getInstance() {
        if (fragment == null) {
            fragment = new LoadingFragment();

        }
        return fragment;
    }

}
