package com.mitross.weatherforecast.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mitross.weatherforecast.datalayers.model.ForecastDataResponse;
import com.mitross.weatherforecast.datalayers.retrofit.ApiInterface;
import com.mitross.weatherforecast.datalayers.retrofit.RetrofitProvider;
import com.mitross.weatherforecast.interfaces.FragmentController;
import com.mitross.weatherforecast.utils.StaticData;
import com.mitross.weatherforecast.utils.StaticUtils;

import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class BaseFragment extends Fragment {

    private static final String TAG = BaseFragment.class.getSimpleName();

    Unbinder unbinder;
    private FragmentController fragmentController;


    public BaseFragment() {
        // Required empty public constructor
    }

    public abstract Integer layoutId();

    @Override
    public void onAttach(final Context activity) {
        super.onAttach(activity);

        if (activity instanceof FragmentController) {
            fragmentController = (FragmentController) activity;
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        if (layoutId() == null)
        {
            return super.onCreateView(inflater, container, savedInstanceState);
        }
        View view = inflater.inflate(layoutId(), container, false);
        unbinder = ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void onActivityCreated(final Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
    }


    protected void addFragment(final Fragment fragment) {
        fragmentController.addFragmentToRootContainer(fragment);
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
        unbinder.unbind();
    }

    public boolean onBackPressed()
    {
        return false;
    }

    public void navigateToDifferentScreen(Intent nextScreenIntent){
        navigateToDifferentScreen(nextScreenIntent,null,"",false,false,0,0);
    }


    public void navigateToDifferentScreen(Intent nextScreenIntent,View view,String sharedElementName,boolean finishActivity){
        navigateToDifferentScreen(nextScreenIntent,view,sharedElementName,false,finishActivity,0,0);
    }


    public void navigateToDifferentScreen(Intent nextScreenIntent,View view,String sharedElementName){
        navigateToDifferentScreen(nextScreenIntent,view,sharedElementName,false,false,0,0);
    }


    public void navigateToDifferentScreen(Intent nextScreenIntent,boolean finishActivity){
        navigateToDifferentScreen(nextScreenIntent,null,"",false,finishActivity,0,0);
    }


    public void navigateToDifferentScreen(Intent nextScreenIntent,boolean finishActivity,int startAnimation,int endAnimation){
        navigateToDifferentScreen(nextScreenIntent,null,"",true,finishActivity,startAnimation,endAnimation);
    }


    /**
     * Common method to navigate in different activity class
     * @param nextScreenIntent object of Intent
     * @param view view which will be animate
     * @param sharedElementName transition name
     * @param isAnimate boolean for screen animation
     * @param finishActivity boolean set true if want to finish current activity
     * @param startAnimation this is start animation use full if isAnimate is true
     * @param endAnimation this is end animation use full if isAnimate is true
     */
    public void navigateToDifferentScreen(Intent nextScreenIntent, View view, String sharedElementName,
                                          boolean isAnimate, boolean finishActivity, int startAnimation, int endAnimation){
        try {
            if (view != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ActivityOptionsCompat options = ActivityOptionsCompat.
                        makeSceneTransitionAnimation(getActivity(), view, sharedElementName);
                startActivity(nextScreenIntent, options.toBundle());
                if (finishActivity){
                    getActivity().finish();
                }
            }
            else {
                startActivity(nextScreenIntent);
                if (isAnimate){
                    getActivity().overridePendingTransition(startAnimation,endAnimation);
                }
                if (finishActivity){
                    getActivity().finish();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void callApiToGetForecast(double latitude, double longitude) {
        if (!StaticUtils.isConnectingToInternet(getActivity())) {
            Log.e(TAG, "no internet connection");
            return;
        }

        String latLong = String.valueOf(latitude).concat(",").concat(String.valueOf(longitude));

        HashMap hashMap = new HashMap<String, String>();
        hashMap.put(StaticData.REQUEST_PARAMS_KEY, StaticData.APIUX_KEY);
        hashMap.put(StaticData.REQUEST_PARAMS_Q, latLong);
        hashMap.put(StaticData.REQUEST_PARAMS_DT, StaticUtils.getCurrentDate());
        hashMap.put(StaticData.REQUEST_PARAMS_DAY, "6");

        ApiInterface apiInterface = RetrofitProvider.createService(ApiInterface.class);
        Call apiClassCall = apiInterface.getForecastData(hashMap);
        Log.e(TAG, apiClassCall.request().toString());
        apiClassCall.enqueue(new Callback<ForecastDataResponse>() {
            @Override
            public void onResponse(Call<ForecastDataResponse> call, Response<ForecastDataResponse> response) {
                if (response.body() != null) {
                    try {
                        ForecastDataResponse model = response.body();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ForecastDataResponse> call, Throwable t) {
                Log.e("ic_error", "" + t.getMessage());
            }
        });

    }

}
