package com.mitross.weatherforecast.application;

import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.mitross.weatherforecast.utils.StaticUtils;

/**
 * Created on 08-04-2018.
 */

public class BaseApplication extends MultiDexApplication {

    public static boolean isAppWentToBg = true;

    public static BaseApplication baseApplication;

    @Override
    public void onCreate() {

        super.onCreate();
        baseApplication = this;
        MultiDex.install(this);

        StaticUtils.setWindowDimensions(this);
    }

    public static BaseApplication getInstance(){
        return baseApplication;
    }
}
