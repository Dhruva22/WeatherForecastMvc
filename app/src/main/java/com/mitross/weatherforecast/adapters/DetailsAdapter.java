package com.mitross.weatherforecast.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.mitross.weatherforecast.R;
import com.mitross.weatherforecast.datalayers.model.ForecastDataResponse;
import com.mitross.weatherforecast.datalayers.model.Forecastday;
import com.mitross.weatherforecast.utils.StaticUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailsAdapter extends RecyclerView.Adapter<DetailsAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Forecastday> lstForecastData;

    public DetailsAdapter(Context context, ArrayList<Forecastday> lstForecastData) {
        this.context = context;
        this.lstForecastData = lstForecastData;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View rootView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_detail, parent, false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        if (lstForecastData != null && 0 <= position && position < lstForecastData.size()) {
            holder.item = lstForecastData.get(position);

            if(holder.item.getDate().contains(StaticUtils.getCurrentDate())) {
                holder.tvDate.setText(context.getString(R.string.today));
            } else if(holder.item.getDate().contains(StaticUtils.getTomorrowsDate())) {
                holder.tvDate.setText(context.getString(R.string.tomorrow));
            } else {
                holder.tvDate.setText(StaticUtils.parseDateToddMMMyyyy(holder.item.getDate()));
            }

            String condition = holder.item.getDay().getCondition().getText();
            Drawable conditionDrawable = null;

            switch (condition) {
                case "Clear":
                    conditionDrawable = ContextCompat.getDrawable(context, R.drawable.ic_clear);
                    break;
                case "Mist":
                case "Overcast":
                case "Cloudy":
                case "Partly cloudy":
                    conditionDrawable = ContextCompat.getDrawable(context, R.drawable.ic_clouds);
                    break;
                case "Sunny":
                    conditionDrawable = ContextCompat.getDrawable(context, R.drawable.ic_sun);
                    break;
                case "Patchy rain possible":
                case "Rain":
                    conditionDrawable = ContextCompat.getDrawable(context, R.drawable.ic_rain);
                    break;
                case "Strom":
                    conditionDrawable = ContextCompat.getDrawable(context, R.drawable.ic_storm);
                    break;
            }

            holder.tvAtmosphere.setCompoundDrawables(null,conditionDrawable,null,null);
            holder.tvAtmosphere.setText(condition);

            double min_temp = holder.item.getDay()
                    .getMintemp_c();
            double max_temp = holder.item.getDay()
                    .getMaxtemp_c();
            String temp = String.valueOf(min_temp).concat("°/").concat(String.valueOf(max_temp)).concat("°");
            holder.tvMinMaxTemperature.setText(temp);
        }

    }

    @Override
    public int getItemCount() {
        return lstForecastData.size();
    }

    public void updateList(List<Forecastday> lstForecastData) {
        this.lstForecastData = (ArrayList<Forecastday>) lstForecastData;
        notifyItemRangeInserted(0, lstForecastData.size());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvDate)
        TextView tvDate;
        @BindView(R.id.tvAtmosphere)
        TextView tvAtmosphere;
        @BindView(R.id.tvMinMaxTemperature)
        TextView tvMinMaxTemperature;

        Forecastday item;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
