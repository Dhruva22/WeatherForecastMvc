package com.mitross.weatherforecast.datalayers.model;

public class Astro {

    private String sunrise;
    private String sunset;
    private String moonrise;
    private String moonset;

    /**
     * No args constructor for use in serialization
     *
     */
    public Astro() {
    }

    /**
     *
     * @param moonset
     * @param sunset
     * @param sunrise
     * @param moonrise
     */
    public Astro(String sunrise, String sunset, String moonrise, String moonset) {
        super();
        this.sunrise = sunrise;
        this.sunset = sunset;
        this.moonrise = moonrise;
        this.moonset = moonset;
    }

    public String getSunrise() {
        return sunrise;
    }

    public void setSunrise(String sunrise) {
        this.sunrise = sunrise;
    }

    public String getSunset() {
        return sunset;
    }

    public void setSunset(String sunset) {
        this.sunset = sunset;
    }

    public String getMoonrise() {
        return moonrise;
    }

    public void setMoonrise(String moonrise) {
        this.moonrise = moonrise;
    }

    public String getMoonset() {
        return moonset;
    }

    public void setMoonset(String moonset) {
        this.moonset = moonset;
    }
}
