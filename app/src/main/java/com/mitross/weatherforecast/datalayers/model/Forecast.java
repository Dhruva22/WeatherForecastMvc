package com.mitross.weatherforecast.datalayers.model;

import java.util.List;

public class Forecast {

    private List<Forecastday> forecastday = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public Forecast() {
    }

    /**
     *
     * @param forecastday
     */
    public Forecast(List<Forecastday> forecastday) {
        super();
        this.forecastday = forecastday;
    }

    public List<Forecastday> getForecastday() {
        return forecastday;
    }

    public void setForecastday(List<Forecastday> forecastday) {
        this.forecastday = forecastday;
    }

}
