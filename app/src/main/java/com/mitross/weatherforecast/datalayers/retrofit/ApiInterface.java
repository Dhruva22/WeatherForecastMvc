package com.mitross.weatherforecast.datalayers.retrofit;

import com.mitross.weatherforecast.datalayers.model.ForecastDataResponse;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface ApiInterface {

    @GET("/v1/forecast.json")
    Call<ForecastDataResponse> getForecastData(@QueryMap HashMap<String, String> hashMap);

}


