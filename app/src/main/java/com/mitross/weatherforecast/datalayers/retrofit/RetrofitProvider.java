package com.mitross.weatherforecast.datalayers.retrofit;

import com.mitross.weatherforecast.BuildConfig;
import com.mitross.weatherforecast.utils.StaticData;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitProvider {
    private static Retrofit retrofit;
    private static OkHttpClient okHttpClient;


    private static HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
        @Override
        public void log(String message) {
            // Log.e("response",message);
        }
    }).setLevel(HttpLoggingInterceptor.Level.BODY);


    private static OkHttpClient getHttpClient(){

        if (okHttpClient == null) {
            okHttpClient =  new OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .addInterceptor(loggingInterceptor)
                    .build();
        }
        return okHttpClient;

    }

    /**
     * Method to return retrofit instance. This method will retrofit retrofit instance with app api Base url.
     * @return instance of ad retrofit.
     */

    private static Retrofit getRetrofit() {
        if (retrofit != null){
            return retrofit;
        }else{
            retrofit = new Retrofit.Builder()
                    .baseUrl(StaticData.BASE_URL)
                    .client(getHttpClient())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            return retrofit;
        }

    }

    public static <S> S createService(Class<S> serviceClass) {
        return getRetrofit().create(serviceClass);
    }

}
