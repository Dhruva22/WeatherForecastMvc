package com.mitross.weatherforecast.datalayers.model;

public class Forecastday {

    private String date;
    private int date_epoch;
    private Day day;
    private Astro astro;

    /**
     * No args constructor for use in serialization
     *
     */
    public Forecastday() {
    }

    /**
     *
     * @param astro
     * @param date_epoch
     * @param day
     * @param date
     */
    public Forecastday(String date, int date_epoch, Day day, Astro astro) {
        super();
        this.date = date;
        this.date_epoch = date_epoch;
        this.day = day;
        this.astro = astro;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getDate_epoch() {
        return date_epoch;
    }

    public void setDate_epoch(int date_epoch) {
        this.date_epoch = date_epoch;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public Astro getAstro() {
        return astro;
    }

    public void setAstro(Astro astro) {
        this.astro = astro;
    }

}
