package com.mitross.weatherforecast.activities

import android.annotation.SuppressLint
import android.os.AsyncTask
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.View
import butterknife.OnClick
import com.mitross.weatherforecast.Animations.SlideUpItemAnimator
import com.mitross.weatherforecast.R
import com.mitross.weatherforecast.adapters.DetailsAdapter
import com.mitross.weatherforecast.datalayers.model.ForecastDataResponse
import com.mitross.weatherforecast.datalayers.model.Forecastday
import com.mitross.weatherforecast.datalayers.retrofit.ApiInterface
import com.mitross.weatherforecast.datalayers.retrofit.RetrofitProvider
import com.mitross.weatherforecast.utils.StaticData
import com.mitross.weatherforecast.utils.StaticUtils
import kotlinx.android.synthetic.main.activity_home.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList
import java.util.HashMap

class HomeActivity : BaseActivity() {

    private val TAG = HomeActivity::class.java.simpleName

    private var forecastTask: AsyncTask<*, *, *>? = null
    private var model: ForecastDataResponse? = null
    private val lstForecasrData = ArrayList<Forecastday>()
    private var detailsAdapter: DetailsAdapter? = null
    private var status: Int = 0

    override fun getLayoutId(): Int {
        return R.layout.activity_home
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initActivity()
    }

    private fun initActivity() {
        context = this@HomeActivity
        setUpAdapter()
        val countDownTimer = object : CountDownTimer(500, 500) {
            override fun onTick(millisUntilFinished: Long) {
                setLoadingView()
            }

            override fun onFinish() {
                forecastTask = ForecastTask().execute()
            }
        }
        countDownTimer.start()
    }

    private fun setUpAdapter() {
        detailsAdapter = DetailsAdapter(context, lstForecasrData)
        rvDetails.adapter = detailsAdapter
        rvDetails.itemAnimator = SlideUpItemAnimator()
    }

    @OnClick(R.id.ivEmptyImage)
    fun onViewClicked() {
        setLoadingView()
        forecastTask = if (forecastTask == null || forecastTask!!.isCancelled) {
            ForecastTask().execute()
        } else {
            forecastTask!!.cancel(true)
            ForecastTask().execute()
        }
    }

    @SuppressLint("StaticFieldLeak")
    private inner class ForecastTask : AsyncTask<Void, Void, Int?>() {

        override fun onPreExecute() {
            super.onPreExecute()
            setLoadingView()
        }

        override fun doInBackground(vararg voids: Void): Int? {
            return callApiToGetForecast(23.02, 72.57)
        }

        override fun onPostExecute(aVoid: Int?) {
            when (aVoid) {
                1 -> setNoNetworkView()
                2 -> setErrorView()
            }
        }
    }

    /**
     * Method making api call and getting data
     *
     * @param latitude  latitude of any location
     * @param longitude longitude of any location
     */
    fun callApiToGetForecast(latitude: Double, longitude: Double): Int {

        if (!StaticUtils.isConnectingToInternet(this)) {
            status = 1
            return status
        }

        val latLong = latitude.toString() + "," + longitude.toString()

        val hashMap = HashMap<String, String>()
        hashMap[StaticData.REQUEST_PARAMS_Q] = latLong
        hashMap[StaticData.REQUEST_PARAMS_KEY] = StaticData.APIUX_KEY
        hashMap[StaticData.REQUEST_PARAMS_DAY] = "7"
        hashMap[StaticData.REQUEST_PARAMS_DT] = StaticUtils.getCurrentDate()

        val apiInterface = RetrofitProvider.createService(ApiInterface::class.java)
        val apiClassCall = apiInterface.getForecastData(hashMap)
        Log.e(TAG, apiClassCall.request().toString())
        apiClassCall.enqueue(object : Callback<ForecastDataResponse> {
            override fun onResponse(call: Call<ForecastDataResponse>, response: Response<ForecastDataResponse>) {
                if (response.body() != null) {
                    try {
                        model = response.body()
                        if (model != null) {
                            rlLoading.visibility = View.GONE
                            clDetails.visibility = View.VISIBLE
                            tvCurrentTemp.text = model!!.current.temp_c.toInt().toString() + "°"

                            tvCity.text = model!!.location.name
                            detailsAdapter!!.updateList(model!!.forecast.forecastday)

                            status = 0


                        } else {
                            status = 2
                            //setErrorView()
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                        status = 2
                        //setErrorView()

                    }

                }
            }

            override fun onFailure(call: Call<ForecastDataResponse>, t: Throwable) {
                Log.e("ic_error", "" + t.message)
                status = 2
                //setErrorView()
            }
        })

        return status

    }

    private fun setNoNetworkView() {
        lottieLoading.setVisibility(View.INVISIBLE)
        ivEmptyImage.visibility = View.VISIBLE
        tvStatusText.text = getString(R.string.no_network)
        tvStatusText.background = null
        tvStatusText.isClickable = false
    }

    private fun setErrorView() {
        lottieLoading.visibility = View.INVISIBLE
        ivEmptyImage.visibility = View.VISIBLE
        tvStatusText.text = getString(R.string.retry)
        tvStatusText.setBackgroundResource(R.drawable.ic_retry_btn)
        tvStatusText.isClickable = true
    }

    private fun setLoadingView() {
        lottieLoading.visibility = View.VISIBLE
        ivEmptyImage.visibility = View.INVISIBLE
        tvStatusText.text = getString(R.string.loading)
        tvStatusText.background = null
        tvStatusText.isClickable = false
    }

    override fun onDestroy() {
        if(forecastTask != null) {
            forecastTask!!.cancel(true)
            forecastTask = null
        }
        super.onDestroy()
    }

}