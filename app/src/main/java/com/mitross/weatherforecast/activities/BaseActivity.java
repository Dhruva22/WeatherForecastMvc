package com.mitross.weatherforecast.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.mitross.weatherforecast.R;
import com.mitross.weatherforecast.fragments.LoadingFragment;
import com.mitross.weatherforecast.utils.PopUtils;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseActivity extends AppCompatActivity {

    protected abstract Integer getLayoutId();

    public Context context;
    Unbinder unbinder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getLayoutId() == null) {
            return;
        }
        setContentView(getLayoutId());
        //we are importing unbinber and binder in base activity.
        // so no need to import it in child classes.
        unbinder = ButterKnife.bind(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        context = this;
    }

    @SuppressWarnings("unused")
    public void navigateToDifferentScreen(Intent nextScreenIntent) {
        navigateToDifferentScreen(nextScreenIntent, null, "", false, false, 0, 0);

    }

    @SuppressWarnings("unused")
    public void navigateToDifferentScreen(Intent nextScreenIntent, View view, String sharedElementName, Boolean finishActivity) {
        navigateToDifferentScreen(nextScreenIntent, view, sharedElementName, false, finishActivity, 0, 0);

    }

    @SuppressWarnings("unused")
    public void navigateToDifferentScreen(Intent nextScreenIntent, View view, String sharedElementName) {
        navigateToDifferentScreen(nextScreenIntent, view, sharedElementName, false, false, 0, 0);

    }

    @SuppressWarnings("unused")
    public void navigateToDifferentScreen(Intent nextScreenIntent, Boolean finishActivity) {
        navigateToDifferentScreen(nextScreenIntent, null, "", false, finishActivity, 0, 0);

    }

    @SuppressWarnings("unused")
    public void navigateToDifferentScreen(Intent nextScreenIntent, Boolean finishActivity, int startAnimation, int endAnimation) {
        navigateToDifferentScreen(nextScreenIntent, null, "", true, finishActivity, startAnimation, endAnimation);

    }

    /**
     * Common method to navigate in different activity class
     * @param nextScreenIntent object of Intent
     * @param view view which will be animate
     * @param sharedElementName transition name
     * @param isAnimate boolean for screen animation
     * @param finishActivity boolean set true if want to finish current activity
     * @param startAnimation this is start animation use full if isAnimate is true
     * @param endAnimation this is end animation use full if isAnimate is true
     */
    public void navigateToDifferentScreen(Intent nextScreenIntent, View view, String sharedElementName,
                                  boolean isAnimate, boolean finishActivity, int startAnimation, int endAnimation) {

        try {
            if (view != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, view, sharedElementName);
                startActivity(nextScreenIntent, options.toBundle());
                if (finishActivity) {
                    finish();
                }
            } else {
                startActivity(nextScreenIntent);
                if (isAnimate) {
                    overridePendingTransition(startAnimation, endAnimation);
                }

                if (finishActivity) {
                    finish();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void finishActivity() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                supportFinishAfterTransition();
            } else {
                finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @SuppressWarnings("unused")
    public void onBackPressFromActivity() {
        onBackPressFromActivity(null, false);
    }

    @SuppressWarnings("unused")
    public void onBackPressFromActivity(boolean isHomeScreen) {
        onBackPressFromActivity(null, isHomeScreen);
    }

    private void onBackPressFromActivity(Intent nextScreenIntent, boolean isHomeScreen) {

        try {
            if (isHomeScreen) {
                PopUtils.showCustomTwoButtonAlertDialog(this, "",
                        getString(R.string.app_name),
                        getString(R.string.yes),
                        getString(R.string.no), false,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //On yes click
                                dialog.dismiss();
                                finishActivity();
                            }
                        }, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //On no click
                                dialog.dismiss();
                            }
                        });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /*public void addFragment(Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager()
                .beginTransaction();
        ft.add(R.id.flFragmentContainer, fragment, LoadingFragment.TAG);
    }

    public void replaceFragment(Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager()
                .beginTransaction();
        // flFragmentContainer is your FrameLayout container
        ft.replace(R.id.flFragmentContainer, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(null);
        ft.commit();
    }*/

}
